FROM cypress/browsers:chrome69
MAINTAINER Aage Dahl

RUN echo "deb http://http.debian.net/debian jessie-backports main" > /etc/apt/sources.list.d/jessie-backports.list
RUN apt-get update
RUN apt-get -y install -t jessie-backports openjdk-8-jdk ssh

WORKDIR /app
