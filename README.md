# cypress-chrome-69
Docker image based on cypress/browsers:chrome69 with Java (inheriting Gradle and node from base image).

# Versions
- OpenJDK-8-JDK
#Inherited versions
- Node 10.11.0
- Gradle 6.4.1
